from flask import Flask 
from flask import render_template
from pymongo import MongoClient, DESCENDING

client = MongoClient()
db = client.test

app = Flask(__name__)
cursor = db.zips.find().sort('pop', DESCENDING)[:20]

@app.route('/')
def chart():
	f = lambda x: x
	cities = list(map(f, cursor)) 

	for city in cities: 
		for key in city.keys():
			if key == 'pop': 
				city['population'] = city[key] 
				del city['pop']

	return render_template('index.html', cities=cities)

if __name__ == '__main__':
	app.run()
